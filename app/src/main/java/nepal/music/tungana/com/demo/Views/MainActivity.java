package nepal.music.tungana.com.demo.Views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import nepal.music.tungana.com.demo.Controllers.MyActivityController;
import nepal.music.tungana.com.demo.Interfaces.MainActivityinterface;
import nepal.music.tungana.com.demo.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MainActivityinterface {
    MyActivityController myActivityController;
    EditText username, password;
    Button loginBtn;
    SharedPreferences loginPreferences;
    public static final String LOGIN_STATUS = "LoginStatus";
    boolean status;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);
        // initialize username and password
        loginPreferences = getPreferences (Context.MODE_PRIVATE);
        status = loginPreferences.getBoolean (LOGIN_STATUS, false);
        if(status == false){
            Toast.makeText (getApplicationContext (), "Still not logged in", Toast.LENGTH_SHORT).show ();
        }
        else{
            Intent intent = new Intent(MainActivity.this, DashBoardActivity.class);
            startActivity (intent);
        }
        myActivityController = new MyActivityController(MainActivity.this);

        username = (EditText)findViewById (R.id.editUsername);
        password = (EditText)findViewById (R.id.editPassword);

        loginBtn = (Button) findViewById (R.id.btnLogin);
        loginBtn.setOnClickListener (this);

    }


    @Override
    public void onClick (View view) {
        switch (view.getId ()){
            case R.id.btnLogin:

                String user = username.getText ().toString ();
                String pass = password.getText ().toString ();
                myActivityController.loginValidation (user, pass);

                break;
        }
    }

    @Override
    public void loginSuccess (boolean status) {
       Toast.makeText (getApplicationContext (), "Username and password matched"+" "+status, Toast.LENGTH_SHORT).show ();
        SharedPreferences.Editor editor = loginPreferences.edit ();
        editor.putBoolean (LOGIN_STATUS, status);
        editor.commit ();
        Intent intent = new Intent(MainActivity.this, DashBoardActivity.class);
        startActivity (intent);
    }

    @Override
    public void loginFail (boolean status) {
        Toast.makeText (getApplicationContext (), "Wrong username and/or password"+" "+status, Toast.LENGTH_SHORT).show ();
        SharedPreferences.Editor editor = loginPreferences.edit ();
        editor.putBoolean (LOGIN_STATUS, status);
        editor.commit ();
    }
}
