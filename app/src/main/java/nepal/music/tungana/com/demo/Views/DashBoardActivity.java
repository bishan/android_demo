package nepal.music.tungana.com.demo.Views;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import nepal.music.tungana.com.demo.R;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener{
    SharedPreferences loginPreferences;
    public static final String LOGIN_STATUS = "LoginStatus";
    Button btnLogout;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_dash_board);
        loginPreferences = getPreferences (Context.MODE_PRIVATE);

        btnLogout = (Button)findViewById (R.id.logout);
        btnLogout.setOnClickListener (this);
    }

    @Override
    public void onClick (View view) {
        switch (view.getId ()){
            case R.id.logout:
                SharedPreferences.Editor editor = loginPreferences.edit ();
                editor.putBoolean (LOGIN_STATUS, false);
                editor.commit ();
                this.finish ();
                break;
        }
    }
}
